<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>App Rent - Возьми приложение в аренду</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;700&display=swap">
</head>
<body style="font-family: 'Roboto', sans-serif; background-color: #f8f9fa;">

<div class="container mt-5">
    <div class="card shadow">
        <div class="card-body">
            <h2 class="card-title text-center mb-4">Форма заявки</h2>
            <form id="subscribeForm" action="{{ url('/api/subscribe') }}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="mb-3">
                    <label for="logo" class="form-label">Логотип</label>
                    <input type="file" class="form-control" id="logo" name="logo">
                </div>
                <div class="mb-3">
                    <label for="name" class="form-label">Название</label>
                    <input type="text" class="form-control" id="name" name="name" required>
                </div>
                <div class="mb-3">
                    <label for="additional_info" class="form-label">Информация про продукт</label>
                    <textarea class="form-control" id="additional_info" name="additional_info" rows="3"></textarea>
                </div>
                <div class="mb-3">
                    <label for="telegram" class="form-label">Телеграм</label>
                    <input type="text" class="form-control" id="telegram" name="telegram" maxlength="255">
                </div>
                <div class="mb-3">
                    <label for="whatsapp" class="form-label">WhatsApp</label>
                    <input type="text" class="form-control" id="whatsapp" name="whatsapp" maxlength="255">
                </div>
                <div class="mb-3">
                    <label for="phone" class="form-label">Номер телефона</label>
                    <input type="text" class="form-control" id="phone" name="phone" required maxlength="20">
                </div>

                <div class="mb-3">
                    <label for="product_type_id" class="form-label">Тип продукта</label>
                    <select class="form-select" id="product_type_id" name="product_type_id">
                        <option value="">Выберите тип продукта</option>
                        @foreach ($productTypes as $productType)
                            <option value="{{ $productType->id }}">{{ $productType->product_name }}</option>
                        @endforeach
                    </select>
                </div>

                <button type="submit" class="btn btn-primary">Отправить заявку</button>
            </form>
        </div>
    </div>
</div>

<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
<script>
    $(document).ready(function () {
        $('#subscribeForm').submit(function (e) {
            e.preventDefault();

            if ($('#name').val() === '') {
                alert('Please enter a name.');
                return;
            }

            $.ajax({
                type: 'POST',
                url: $(this).attr('action'),
                data: new FormData(this),
                contentType: false,
                cache: false,
                processData: false,
                success: function (response) {
                    console.log(response);
                    alert('Form submitted successfully!');
                },
                error: function (error) {
                    console.error(error);
                    alert('Form submission failed. Please try again.');
                }
            });
        });
    });
</script>

</body>
</html>
