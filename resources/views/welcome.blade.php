<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>App Rent - Возьми приложение в аренду</title>
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet">
    <style>
        body {
            font-family: 'Arial', sans-serif;
        }

        nav {
            background-color: #fff;
            box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
        }

        .navbar-brand {
            font-weight: bold;
            font-size: 1.5rem;
        }

        header.jumbotron {
            background: url('your-background-image.jpg') center/cover no-repeat;
            color: #fff;
            text-align: center;
            padding: 100px 0;
            margin-bottom: 0;
        }

        header.jumbotron h1 {
            font-size: 3rem;
            font-weight: bold;
        }

        section.container {
            margin-top: 50px;
        }

        section.container h2 {
            font-size: 2.5rem;
            font-weight: bold;
            color: #333;
        }

        section.container p {
            color: #555;
        }

        footer {
            background-color: #f8f9fa;
            color: #555;
        }

        footer a {
            color: #007bff;
        }

    </style>
</head>
<body>

<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="#">Логотип</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNav">
        <ul class="navbar-nav ml-auto">
            <li class="nav-item active">
                <a class="nav-link" href="#">Главная <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">Тарифы</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">О нас</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">Партнеры</a>
            </li>
        </ul>
    </div>
</nav>

<header class="jumbotron">
    <h1 class="display-4">Добро пожаловать в App Rent!</h1>
    <p class="lead">Это инновационный способ управления подписками на различные приложения.</p>
    <hr class="my-4">
    <p>Узнайте больше о наших предложениях и как мы можем помочь вашему бизнесу.</p>
    <a class="btn btn-primary btn-lg" href="#" role="button">Узнать больше</a>
</header>

<section class="container">
    <div class="row">
        <div class="col-md-4 mb-4">
            <h2>Интернет-магазин</h2>
            <p>Создайте свой собственный интернет-магазин легко и быстро.</p>
            <p><a class="btn btn-secondary" href="#" role="button">Подробнее »</a></p>
        </div>
        <div class="col-md-4 mb-4">
            <h2>Онлайн-запись</h2>
            <p>Организуйте систему онлайн-записей для вашего бизнеса.</p>
            <p><a class="btn btn-secondary" href="#" role="button">Подробнее »</a></p>
        </div>
        <div class="col-md-4 mb-4">
            <h2>Онлайн видео-наблюдение</h2>
            <p>Камеры видео-наблюдения с удобным просмотром в реальном времени через приложение.</p>
            <p><a class="btn btn-secondary" href="#" role="button">Подробнее »</a></p>
        </div>
    </div>
</section>

<footer class="text-muted py-4 bg-light">
    <div class="container">
        <p class="float-right">
            <a href="#">Наверх</a>
        </p>
        <p>© 2024 App Rent, Inc. Все права защищены.</p>
    </div>
</footer>

<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.0.6/dist/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>

</body>
</html>
