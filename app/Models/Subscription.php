<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Support\Carbon;

/**
 * Class Subscription
 *
 * @property int $id
 * @property string|null $logo
 * @property string $name
 * @property string|null $additional_info
 * @property string|null $telegram
 * @property string|null $whatsapp
 * @property string|null $phone
 * @property Carbon $subscribed_at
 * @property boolean $is_subscribed
 * @property boolean $auto_subscription_enabled
 * @property int $product_type_id
 * @property ProductType $product
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 * @package App\Models
 */
class Subscription extends Model
{
    use HasFactory;

    protected $fillable = [
        'logo',
        'name',
        'additional_info',
        'telegram',
        'whatsapp',
        'phone',
        'user_id',
        'subscribed_at',
        'is_subscribed',
        'auto_subscription_enabled',
        'product_type_id'
    ];

    public function product(): HasOne
    {
        return $this->hasOne(ProductType::class, 'id', 'product_type_id');
    }
}
