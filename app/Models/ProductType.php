<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * Class ProductType
 *
 * @property int $id
 * @property string $product_name
 * @property string $product_text
 * @property string|null $product_image
 * @property boolean $is_active
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 * @package App\Models
 */
class ProductType extends Model
{
    use HasFactory;

    protected $fillable = [
        'product_name',
        'product_text',
        'product_image',
        'is_active'
    ];
}
