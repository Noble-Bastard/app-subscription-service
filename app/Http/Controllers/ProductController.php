<?php

namespace App\Http\Controllers;

use App\Models\ProductType;
use Illuminate\Database\Query\Builder;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ProductController extends Controller
{
    public function create(Request $request): JsonResponse
    {
        $validatedData = $request->validate([
            'product_name' => 'nullable|string|max:255',
            'product_text' => 'nullable|string',
            'product_image' => 'nullable|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'is_active' => 'nullable|boolean'
        ]);

        if ($request->hasFile('product_image')) {
            $logoPath = $request->file('product_image')->store('product_images', 'public');
            $validatedData['product_image'] = $logoPath;
        }

        /** @var ProductType $productType */
        $productType = ProductType::query()->create($validatedData);

        return response()->json(['message' => 'Successfully created', 'data' => $productType]);
    }


    public function update(Request $request, $id): JsonResponse
    {
        /** @var ProductType $productType */
        $productType = ProductType::query()->findOrFail($id);

        $validatedData = $request->validate([
            'product_name' => 'nullable|string|max:255',
            'product_text' => 'nullable|string',
            'product_image' => 'nullable|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'is_active' => 'nullable|boolean'
        ]);

        if ($request->hasFile('product_image')) {
            Storage::disk('public')->delete($productType->product_image);

            $logoPath = $request->file('product_image')->store('product_images', 'public');
            $validatedData['product_image'] = $logoPath;
        }

        $productType->update($validatedData);

        return response()->json(['message' => 'Successfully updated', 'data' => $productType]);
    }

    public function all(Request $request): JsonResponse
    {
        /** @var ProductType $productType */
        $productType = ProductType::query()
            ->when($request->has('only_active'), function (Builder $query) {
            $query->where('is_active', true);
        })
            ->get();;

        return response()->json(['message' => 'All products types', 'data' => $productType]);
    }
}
