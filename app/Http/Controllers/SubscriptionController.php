<?php

namespace App\Http\Controllers;

use App\Models\ProductType;
use App\Models\Subscription;
use Illuminate\Database\Query\Builder;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class SubscriptionController extends Controller
{
    // 6969647528:AAGjEvg07phR-968kbQSsVc_LN2S6IHy010
    private $chat = 'https://api.telegram.org/bot6969647528:AAGjEvg07phR-968kbQSsVc_LN2S6IHy010/sendMessage?chat_id=-1001914296570&text=';

    public function create(Request $request): JsonResponse
    {
        $validatedData = $request->validate([
            'logo' => 'nullable|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'name' => 'required|string|max:255',
            'additional_info' => 'nullable|string',
            'telegram' => 'nullable|string|max:255',
            'whatsapp' => 'nullable|string|max:255',
            'phone' => 'required|string|max:20',
            'user_id' => 'nullable|integer',
            'product_type_id' => 'nullable|integer'
        ]);

        if ($request->hasFile('logo')) {
            $logoPath = $request->file('logo')->store('logos', 'public');
            $validatedData['logo'] = $logoPath;
        }

        /** @var Subscription $subscription */
        $subscription = Subscription::query()->create($validatedData);

        $this->sendToTelegram($subscription);

        return response()->json(['message' => 'Successfully created', 'data' => $subscription]);
    }

    public function sendToTelegram(Subscription $subscription)
    {
        $message = "New subscription request!\n\n";
        $message .= "Name: " . $subscription->name . "\n";
        $message .= "Additional Info: " . $subscription->additional_info . "\n";
        $message .= "Telegram: " . $subscription->telegram . "\n";
        $message .= "WhatsApp: " . $subscription->whatsapp . "\n";
        $message .= "Phone: " . $subscription->phone . "\n";
        $message .= "Product type: " . $subscription->product->product_name. "\n";


        return file_get_contents($this->chat . urlencode($message));
    }

    public function update(Request $request, $id): JsonResponse
    {
        /** @var Subscription $subscription */
        $subscription = Subscription::query()->findOrFail($id);

        $validatedData = $request->validate([
            'name' => 'nullable|string|max:255',
            'additional_info' => 'nullable|string',
            'telegram' => 'nullable|string|max:255',
            'whatsapp' => 'nullable|string|max:255',
            'phone' => 'nullable|string|max:20',
            'user_id' => 'nullable|integer',
            'subscribed_at' => 'nullable|date',
            'is_subscribed' => 'nullable|boolean',
            'auto_subscription_enabled' => 'nullable|boolean'
        ]);

        if ($request->hasFile('logo')) {
            Storage::disk('public')->delete($subscription->logo);

            $logoPath = $request->file('logo')->store('logos', 'public');
            $validatedData['logo'] = $logoPath;
        }

        $subscription->update($validatedData);

        return response()->json(['message' => 'Successfully updated', 'data' => $subscription]);
    }

    public function all(Request $request): JsonResponse
    {
        /** @var Subscription $subscription */
        $subscription = Subscription::query()
            ->when($request->has('only_active'), function (Builder $query) {
                $query->where('is_subscribed', true);
            })
            ->get();

        return response()->json(['message' => 'All subscription', 'data' => $subscription]);
    }

    public function submitForm()
    {
        $productTypes = ProductType::all();
        return view('submitForm', compact('productTypes'));
    }
}
