<?php

use App\Http\Controllers\ProductController;
use App\Http\Controllers\SubscriptionController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::put('/subscribe/{id}', [SubscriptionController::class, 'update']);
Route::post('/subscribe', [SubscriptionController::class, 'create']);
Route::post('/subscribe/all', [SubscriptionController::class, 'all']);


Route::put('/product-type/{id}', [ProductController::class, 'update']);
Route::post('/product-type', [ProductController::class, 'create']);
Route::post('/product-type/all', [ProductController::class, 'all']);

